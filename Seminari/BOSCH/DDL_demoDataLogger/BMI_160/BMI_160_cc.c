/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file        
 *
 * Demo application of printing BMI160 Accel and Gyro data on serialport(USB virtual comport)
 * every one second, initiated by auto-reloaded timer(freeRTOS)
 * 
 * ****************************************************************************/

/* module includes ********************************************************** */

/* own header files */
#include "XdkSensorHandle.h"
#include "XDK_Datalogger_ih.h"
#include "XDK_Datalogger_ch.h"
#include "BMI_160_ih.h"
#include "BMI_160_ch.h"

/* system header files */
#include <stdio.h>
#include <BCDS_Basics.h>

/* additional interface header files */
#include "BCDS_BSP_LED.h"
#include "BSP_BoardType.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_Accelerometer.h"
#include "BCDS_Gyroscope.h"
#include "BCDS_Retcode.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
/* global variables ********************************************************* */
Accelerometer_XyzData_T getAccelDataRaw160 = { INT32_C(0), INT32_C(0), INT32_C(0) };
Accelerometer_XyzData_T getAccelDataUnit160 = { INT32_C(0), INT32_C(0), INT32_C(0) };
Gyroscope_XyzData_T getGyroDataRaw160 = { INT32_C(0), INT32_C(0), INT32_C(0) };
Gyroscope_XyzData_T getGyroDataConv160 = { INT32_C(0), INT32_C(0), INT32_C(0) };
Gyroscope_Bandwidth_T bmi160gyro_bw = GYROSCOPE_BANDWIDTH_OUT_OF_RANGE;
Accelerometer_Bandwidth_T bmi160accel_bw = ACCELEROMETER_BANDWIDTH_OUT_OF_RANGE;
Accelerometer_Range_T bmi160range = ACCELEROMETER_RANGE_OUT_OF_BOUND;
/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/** The function to get and print the accel data using printf
 * @brief Gets the data from BMI160 Accel and BMI160 Gyro
 *
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
void bmi160_getSensorValues(void *pvParameters)
{

    /* Return value for Accel Sensor */
    Retcode_T accelReturnValue = (Retcode_T) RETCODE_FAILURE;
    Retcode_T gyroReturnValue = (Retcode_T) RETCODE_FAILURE;
    (void) pvParameters;

    accelReturnValue = Accelerometer_readXyzLsbValue(xdkAccelerometers_BMI160_Handle, &getAccelDataRaw160);
    if (RETCODE_OK != accelReturnValue)
    {
          printf("BMI160 Read Raw Data Failed\n\r");
    }
    accelReturnValue = Accelerometer_readXyzGValue(xdkAccelerometers_BMI160_Handle, &getAccelDataUnit160);
    if (RETCODE_OK != accelReturnValue)
    {
         printf("BMI160 Read G Data Failed\n\r");
    }
    gyroReturnValue = Gyroscope_readXyzValue(xdkGyroscope_BMI160_Handle, &getGyroDataRaw160);
    if (RETCODE_OK != gyroReturnValue)
    {
          printf("BMI160 GyrosensorReadRawData Failed\n\r");
    }
    /* read sensor data in milli Degree*/
    gyroReturnValue = Gyroscope_readXyzDegreeValue(xdkGyroscope_BMI160_Handle, &getGyroDataConv160);
    if (RETCODE_OK != gyroReturnValue)
    {
          printf("BMI160 GyrosensorReadInMilliDeg Failed\n\r");
    }
}
/* global functions ********************************************************* */
/**
 * @brief The function initializes BMI(Interial-accel & gyro) and set sensor parameter from logger.ini
 *
 */
extern void bmi_160_init(void)
{
    /* Return value for Accel Sensor */
    Retcode_T accelReturnValue = (Retcode_T) RETCODE_FAILURE;
    /* Return value for Accel Sensor */
    Retcode_T gyroReturnValue = (Retcode_T) RETCODE_FAILURE;
    /*initialize accel*/
    Retcode_T returnVal = RETCODE_OK;
    accelReturnValue = Accelerometer_init(xdkAccelerometers_BMI160_Handle);
    gyroReturnValue = Gyroscope_init(xdkGyroscope_BMI160_Handle);
    if ((RETCODE_OK == accelReturnValue)
            && (RETCODE_OK == gyroReturnValue))
    {
        printf("BMI160 Init Succeed\n\r");
    }
    else
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }
        printf("BMI160 Init failed\n\r");
    }

    if (config.bmi160_range == 2)
    {
        bmi160range = ACCELEROMETER_BMI160_RANGE_2G;
    }
    else if (config.bmi160_range == 4)
    {
        bmi160range = ACCELEROMETER_BMI160_RANGE_4G;
    }
    else if (config.bmi160_range == 8)
    {
        bmi160range = ACCELEROMETER_BMI160_RANGE_8G;
    }
    else if (config.bmi160_range == 16)
    {
        bmi160range = ACCELEROMETER_BMI160_RANGE_16G;
    }
    accelReturnValue = Accelerometer_setRange(xdkAccelerometers_BMI160_Handle,
            bmi160range);
    if ((RETCODE_OK != accelReturnValue)
            || (ACCELEROMETER_RANGE_OUT_OF_BOUND == bmi160range))
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }
    }

    if (strcmp(bmi160_accel_bw, "0.39") == 0)
    {
        bmi160accel_bw = ACCELEROMETER_BMI160_BANDWIDTH_0_39HZ;
    }
    else if (strcmp(bmi160_accel_bw, "0.78") == 0)
    {
        bmi160accel_bw = ACCELEROMETER_BMI160_BANDWIDTH_0_78HZ;
    }
    else if (strcmp(bmi160_accel_bw, "1.56") == 0)
    {
        bmi160accel_bw = ACCELEROMETER_BMI160_BANDWIDTH_1_56HZ;
    }
    else if (strcmp(bmi160_accel_bw, "3.12") == 0)
    {
        bmi160accel_bw = ACCELEROMETER_BMI160_BANDWIDTH_3_12HZ;
    }
    if (strcmp(bmi160_accel_bw, "6.25") == 0)
    {
        bmi160accel_bw = ACCELEROMETER_BMI160_BANDWIDTH_6_25HZ;
    }
    if (strcmp(bmi160_accel_bw, "12.5") == 0)
    {
        bmi160accel_bw = ACCELEROMETER_BMI160_BANDWIDTH_12_5HZ;
    }
    if (strcmp(bmi160_accel_bw, "25") == 0)
    {
        bmi160accel_bw = ACCELEROMETER_BMI160_BANDWIDTH_25HZ;
    }
    if (strcmp(bmi160_accel_bw, "50") == 0)
	{
		bmi160accel_bw = ACCELEROMETER_BMI160_BANDWIDTH_50HZ;
	}
	if (strcmp(bmi160_accel_bw, "100") == 0)
	{
		bmi160accel_bw = ACCELEROMETER_BMI160_BANDWIDTH_100HZ;
	}
	if (strcmp(bmi160_accel_bw, "200") == 0)
	{
		bmi160accel_bw = ACCELEROMETER_BMI160_BANDWIDTH_200HZ;
	}
    accelReturnValue = Accelerometer_setBandwidth(
            xdkAccelerometers_BMI160_Handle, bmi160accel_bw);

    if ((RETCODE_OK != accelReturnValue)
            || (ACCELEROMETER_BANDWIDTH_OUT_OF_RANGE == bmi160accel_bw))
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }
    }

    /*Set gyroscpe value */
    if (strcmp(bmi160_gyro_bw, "10.7") == 0)
    {
        bmi160gyro_bw = GYROSCOPE_BMI160_BANDWIDTH_10_7HZ;
    }
    else if (strcmp(bmi160_gyro_bw, "20.8") == 0)
    {
        bmi160gyro_bw = GYROSCOPE_BMI160_BANDWIDTH_20_8HZ;
    }
    else if (strcmp(bmi160_gyro_bw, "39.9") == 0)
    {
        bmi160gyro_bw = GYROSCOPE_BMI160_BANDWIDTH_39_9HZ;
    }
    else if (strcmp(bmi160_gyro_bw, "74.6") == 0)
    {
        bmi160gyro_bw = GYROSCOPE_BMI160_BANDWIDTH_74_6HZ;
    }
    else if (strcmp(bmi160_gyro_bw, "136.6") == 0)
    {
        bmi160gyro_bw = GYROSCOPE_BMI160_BANDWIDTH_136_6HZ;
    }
    else if (strcmp(bmi160_gyro_bw, "254.6") == 0)
    {
        bmi160gyro_bw = GYROSCOPE_BMI160_BANDWIDTH_254_6HZ;
    }
    else if (strcmp(bmi160_gyro_bw, "523.9") == 0)
    {
        bmi160gyro_bw = GYROSCOPE_BMI160_BANDWIDTH_523_9HZ;
    }
    else if (strcmp(bmi160_gyro_bw, "890") == 0)
    {
        bmi160gyro_bw = GYROSCOPE_BMI160_BANDWIDTH_890HZ;
    }
    gyroReturnValue = Gyroscope_setBandwidth(xdkGyroscope_BMI160_Handle, bmi160gyro_bw);
    if ((RETCODE_OK != gyroReturnValue)
            || (GYROSCOPE_BANDWIDTH_OUT_OF_RANGE == bmi160gyro_bw))
    {
        returnVal = BSP_LED_Switch(BSP_XDK_LED_R, BSP_LED_COMMAND_ON);
        if (RETCODE_OK != returnVal)
        {
            printf("Turning on of RED LED failed");
        }
    }

}
/**
 *  @brief API to Deinitialize the PID module
 */
extern void bmi160_deInit(void)
{
    /* Return value for Accel Sensor */
    Retcode_T accelReturnValue = RETCODE_FAILURE;

    /* Return value for Accel Sensor */
    Retcode_T gyroReturnValue = RETCODE_FAILURE;

    /*initialize accel*/
    accelReturnValue = Accelerometer_deInit(xdkAccelerometers_BMI160_Handle);
    /*initialize gyro*/
    gyroReturnValue = Gyroscope_deInit(xdkGyroscope_BMI160_Handle);

    if ((RETCODE_OK == accelReturnValue) && (RETCODE_OK == gyroReturnValue))
    {
        printf("InertialSensor Deinit Success\n\r");
    }
    else
    {
        printf("InertialSensor Deinit Failed\n\r");
    }
}

/** ************************************************************************* */
