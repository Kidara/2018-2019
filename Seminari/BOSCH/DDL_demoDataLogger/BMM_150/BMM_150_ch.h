/******************************************************************************/
/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2015.
 * The use of this software is subject to the XDK SDK EULA
 */
/**
 *  @file
 *
 *
 *  configuration header for the BMM_150 file.
 *
 * ****************************************************************************/

/* header definition ******************************************************** */

#ifndef BMM_150_CH_H_
#define BMM_150_CH_H_

/* local interface declaration ********************************************* */
#include "FreeRTOS.h"
#include "timers.h"

/* local type and macro definitions */

#define PMD_TIMERDELAY       UINT32_C(3000)          /** Three second delay is represented by this macro */
#define PMD_TIMERBLOCKTIME   UINT32_C(0xffff)    	 /** Macro used to define blocktime of a timer*/
#define PMD_ZEROVALUE        UINT32_C(0x00)          /** Macro used to define default value*/

/* local function prototype declarations */
void bmm150_getSensorValues(xTimerHandle pxTimer);
/* local global variable declarations */

/* inline function definitions */

#endif /* BMM_150_CH_H_ */

/** ************************************************************************* */
